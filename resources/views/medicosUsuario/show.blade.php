@extends('plantillaUsuarios.principal')
@section('contenidoUsuarios')
    <link rel="stylesheet" type="text/css" href="{{ asset('assetsU/styles/news.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('form/style2.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assetsU/styles/news_responsive.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assetsU/styles/services.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assetsU/styles/services_responsive.css') }}">

    <div class="home">
        <div class="elementor-kit-2891">
            <img class="mi-img" src="{{ asset('images/medicine2.svg') }}" alt="" height="700">
        </div>
        <div class="mi-home_container">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="home_content">
                            <div class="home_title">Médico</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="news">
        <div class="container">
            <div class="tabs_container">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="tabs d-flex flex-row align-items-center justify-content-start flex-wrap">
                                <div class="tab active">
                                    <div class="tab_title">Médico</div>
                                    <div class="tab_text">Dispuesto a atenderle con el mayor agrado</div>
                                </div>
                                <div class="tab">
                                    <div class="tab_title">Especialidad</div>
                                    <div class="tab_text">Los mejores especialistas de la capital.</div>
                                </div>
                                <div class="tab">
                                    <div class="tab_title">Horarios</div>
                                    <div class="tab_text">Reserve su cita en los horarios disponibles.</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <div class="tab_panels">
                                <div class="tab_panel active">
                                    <div class="tab_panel_content">
                                        <div class="row">
                                            @foreach ($medicos as $medico)
                                                <div class="col-lg-5">
                                                    <div class="tab_image"><img src="{{ asset('images/male.svg') }}"
                                                            alt=""></div>
                                                </div>
                                                <div class="col-lg-7">
                                                    <div class="tab_list">
                                                        <ul>
                                                            <li>
                                                                <div class="tab_list_title">Nombre</div>
                                                                <div class="tab_list_text">
                                                                    <p>{{ $medico->nombre }}.</p>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="tab_list_title">Especialidad</div>
                                                                <div class="tab_list_text">
                                                                    <p>{{ $medico->especialidades_nombre }}.</p>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="tab_list_title">Dirección</div>
                                                                <div class="tab_list_text">
                                                                    <p>{{ $medico->direccion }}.</p>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="tab_panel">
                                    <div class="tab_panel_content">
                                        <div class="tab_list">
                                            <ul>
                                                <li>
                                                    <div class="tab_list_title">Especilidades</div>
                                                    <div class="tab_list_text">
                                                        <p>{{ $medico->especialidades_nombre }}.</p>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab_panel">
                                    <div class="tab_panel_content">
                                        <div class="tab_list">
                                            <ul>
                                                <li>
                                                    <div class="tab_list_title">Horario</div>
                                                    <div class="tab_list_text">
                                                        <p>El médico especialista, le atendera en los siguientes horarios.
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-10 footer_col mt-5">
                                                        <div class="footer_hours">
                                                            <div class="footer_hours_title">Horarios de Atencion</div>
                                                            <ul class="hours_list">
                                                                @foreach ($horarios as $horario)
                                                                    <li
                                                                        class="d-flex flex-row align-items-center justify-content-start">
                                                                        <div> {{ $horario->dia }}</div>
                                                                        <div class="ml-auto">
                                                                            {{ $horario->hora_entrada }}-{{ $horario->hora_salida }}
                                                                        </div>
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 mt-5">
                            <div class="section_title text-center">Reservar Cita</div>
                            <div class="section_subtitle text-center">Puede reservar una cita con este medico y esta
                                especialidad seleccionando el horario que este disponible </div>
                        </div>

                        <div class="col-12 mb-5">
                            <div class="appointment-popup">
                                <div class="appointment-calendar">
                                    <div class="current-week">
                                        <span><i class="fa fa-calendar text-success"></i>18 junio - 25 junio,
                                            2021</span>
                                        <div class="calendar-nav">
                                            <button type="button" class="prev">Prev</button>
                                            <button type="button" class="next">Next</button>
                                        </div>
                                    </div>
                                    <div class="calendar-wrapper">
                                        <div class="calendar-week">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">lunes</th>
                                                        <th scope="col">martes</th>
                                                        <th scope="col">miercoles</th>
                                                        <th scope="col">jueves</th>
                                                        <th scope="col">viernes</th>
                                                        <th scope="col">sabado</th>
                                                        <th scope="col">domingo</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        @foreach ($horarios as $horario)
                                                            @if ($horario->hora_entrada == '08:00:00')
                                                                <td>
                                                                    @if ($horario->estado == 0)
                                                                        <div class="button services_button my-3">
                                                                            <a href="#" class="" type="button"
                                                                                data-toggle="modal"
                                                                                data-target="#exampleModalCenter"
                                                                                onclick="reservar('{{ $horario->dia }}','{{ $horario->hora_entrada }}')"><span>{{ $horario->hora_entrada }}
                                                                                </span><span class="small">Reservar
                                                                                    Cita</span></a>
                                                                        </div>
                                                                    @elseif ($horario->estado == 1)
                                                                        <button class="btn btn-danger text-small my-3">
                                                                            <span>{{ $horario->hora_entrada }}</span></button>
                                                                    @endif
                                                                </td>
                                                            @endif
                                                        @endforeach
                                                    </tr>
                                                    <tr>
                                                        @foreach ($horarios as $horario)
                                                            @if ($horario->hora_entrada == '09:00:00')
                                                                <td>
                                                                    @if ($horario->estado == 0)
                                                                        <div class="button services_button my-3">
                                                                            <a class="" type="button" data-toggle="modal"
                                                                            data-target="#exampleModalCenter"
                                                                                onclick="reservar('{{ $horario->dia }}','{{ $horario->hora_entrada }}')"><span>{{ $horario->hora_entrada }}
                                                                                </span><span class="small">Reservar
                                                                                    Cita</span></a>
                                                                        </div>
                                                                    @elseif ($horario->estado == 1)
                                                                        <button class="btn btn-danger text-small my-3">
                                                                            <span>{{ $horario->hora_entrada }}</span></button>
                                                                    @endif
                                                                </td>
                                                            @endif
                                                        @endforeach
                                                    </tr>
                                                    <tr>
                                                        @foreach ($horarios as $horario)
                                                            @if ($horario->hora_entrada == '10:00:00')
                                                                <td>
                                                                    @if ($horario->estado == 0)
                                                                        <div class="button services_button my-3">
                                                                            <a href="#" class="" type="button"
                                                                                data-toggle="modal"
                                                                                data-target="#exampleModalCenter"
                                                                                onclick="reservar('{{ $horario->dia }}','{{ $horario->hora_entrada }}')"><span>{{ $horario->hora_entrada }}
                                                                                </span><span class="small">Reservar
                                                                                    Cita</span></a>
                                                                        </div>
                                                                    @elseif ($horario->estado == 1)
                                                                        <button class="btn btn-danger text-small my-3">
                                                                            <span>{{ $horario->hora_entrada }}</span></button>
                                                                    @endif
                                                                </td>
                                                            @endif
                                                        @endforeach
                                                    </tr>
                                                    <tr>
                                                        @foreach ($horarios as $horario)
                                                            @if ($horario->hora_entrada == '11:00:00')
                                                                <td>
                                                                    @if ($horario->estado == 0)
                                                                        <div class="button services_button my-3">
                                                                            <a href="#" class="" type="button"
                                                                                data-toggle="modal"
                                                                                data-target="#exampleModalCenter"
                                                                                onclick="reservar('{{ $horario->dia }}','{{ $horario->hora_entrada }}')"><span>{{ $horario->hora_entrada }}
                                                                                </span><span class="small">Reservar
                                                                                    Cita</span></a>
                                                                        </div>
                                                                    @elseif ($horario->estado == 1)
                                                                        <button class="btn btn-danger text-small my-3">
                                                                            <span>{{ $horario->hora_entrada }}</span></button>
                                                                    @endif
                                                                </td>
                                                            @endif
                                                        @endforeach
                                                    </tr>
                                                    <tr>
                                                        @foreach ($horarios as $horario)
                                                            @if ($horario->hora_entrada == '14:00:00')
                                                                <td>
                                                                    @if ($horario->estado == 0)
                                                                        <div class="button services_button my-3">
                                                                            <a href="#" class="" type="button"
                                                                                data-toggle="modal"
                                                                                data-target="#exampleModalCenter"
                                                                                onclick="reservar('{{ $horario->dia }}','{{ $horario->hora_entrada }}')"><span>{{ $horario->hora_entrada }}
                                                                                </span><span class="small">Reservar
                                                                                    Cita</span></a>
                                                                        </div>
                                                                    @elseif ($horario->estado == 1)
                                                                        <button class="btn btn-danger text-small my-3">
                                                                            <span>{{ $horario->hora_entrada }}</span></button>
                                                                    @endif
                                                                </td>
                                                            @endif
                                                        @endforeach
                                                    </tr>
                                                    <tr>
                                                        @foreach ($horarios as $horario)
                                                            @if ($horario->hora_entrada == '15:00:00')
                                                                <td>
                                                                    @if ($horario->estado == 0)
                                                                        <div class="button services_button my-3">
                                                                            <a href="#" class="" type="button"
                                                                                data-toggle="modal"
                                                                                data-target="#exampleModalCenter"
                                                                                onclick="reservar('{{ $horario->dia }}','{{ $horario->hora_entrada }}')"><span>{{ $horario->hora_entrada }}
                                                                                </span><span class="small">Reservar
                                                                                    Cita</span></a>
                                                                        </div>
                                                                    @elseif ($horario->estado == 1)
                                                                        <button class="btn btn-danger text-small my-3">
                                                                            <span>{{ $horario->hora_entrada }}</span></button>
                                                                    @endif
                                                                </td>
                                                            @endif
                                                        @endforeach
                                                    </tr>
                                                    <tr>
                                                        @foreach ($horarios as $horario)
                                                            @if ($horario->hora_entrada == '16:00:00')
                                                                <td>
                                                                    @if ($horario->estado == 0)
                                                                        <div class="button services_button my-3">
                                                                            <a href="#" class="" type="button"
                                                                                data-toggle="modal"
                                                                                data-target="#exampleModalCenter"
                                                                                onclick="reservar('{{ $horario->dia }}','{{ $horario->hora_entrada }}')"><span>{{ $horario->hora_entrada }}
                                                                                </span><span class="small">Reservar
                                                                                    Cita</span></a>
                                                                        </div>
                                                                    @elseif ($horario->estado == 1)
                                                                        <button class="btn btn-danger text-small my-3">
                                                                            <span>{{ $horario->hora_entrada }}</span></button>
                                                                    @endif
                                                                </td>
                                                                {{-- <li>
                                                                    <ul>
                                                                        <li class="empty"></li>
                                                                        <li class="empty"></li>
                                                                    </ul>
                                                                </li> --}}
                                                            @endif
                                                        @endforeach
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-lg  modal-dialog-centered" role="document">
            <div class="modal-content rounded-3">
                <div class="modal-body py-0">


                    <div class="d-block main-content"
                        style="
                                                                                                                                                    background-color: #32c69a;">
                        <img src=" {{ asset('images/cita5.svg') }}" alt="Image" class="img-fluid-1"
                            style="background-color: #32c69a;">
                        <div class="content-text p-4 bg-white">

                            <h3 class="mb-4">Rellena el formulara de cita</h3>
                            <form action="{{ route('medicosUsuario.store') }}" method="POST">
                                {{ csrf_field() }}
                                <div class="pl-lg-4 ml-4">

                                    <div class="row d-none">
                                        <div class="col-lg-10">
                                            <div class="form-group">
                                                <label for="paciente">Email</label>
                                                <input type="text" class="form-control" value="{{ Auth::user()->email }}"
                                                    readonly>
                                                <select class="form-control d-none" id="paciente_id" name="paciente_id">
                                                    @foreach ($pacientes as $paciente)
                                                        <option value="1">
                                                            {{ Auth::user()->email }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-10">
                                            <div class="form-group">
                                                <label for="user">Usuario</label>
                                                <input type="text" class="form-control" value="{{ Auth::user()->name }}"
                                                    readonly>
                                                <select class="form-control d-none" id="user" name="user_id">
                                                    <option value="{{ Auth::user()->id }}">{{ Auth::user()->name }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-10">
                                            <div class="form-group">
                                                <label for="medico">Medico</label>
                                                <input type="text" class="form-control" value="{{ $medico->nombre }}"
                                                    readonly>
                                                <select class="form-control d-none" id="medico" name="medico_id">
                                                    <option value="{{ $medico->id }}">{{ $medico->nombre }}</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="col-lg-10">
                                            <div class="row">
                                                <div class="col-5">
                                                    <input type="text" id="dia" name="fecha_cita" class="form-control " value=""
                                                    readonly>
                                                </div>
                                                <div class="col-5">
                                                    <input type="text" id="hora" name="hora_cita" class="form-control " value="aaa"
                                                    readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-10">
                                            <div class="form-group">
                                                <label class="form-control-label" for="asunto">Asunto</label>
                                                <input type="text" id="asunto" name="asunto" class="form-control"
                                                    placeholder="asunto" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row d-none">
                                        <div class="col-lg-10">
                                            <div class="form-group">
                                                <label for="estadoCita">Estado de Cita</label>
                                                <select class="form-control" id="estadoCita" name="estado_cita">
                                                    <option value="Pendiente">Pendiente</option>
                                                    <option value="Cancelada">Cancelada</option>
                                                    <option value="Activa" selected>Activa</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-10">
                                            <div class="form-group">
                                                <label class="form-control-label" for="precio">Precio</label>
                                                @foreach ($especialidades as $e)

                                                @endforeach
                                                <input type="number" id="precio" name="precio" class="form-control"
                                                    value="{{ $e->precio }}" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row d-none">
                                        <div class="col-lg-10">
                                            <div class="form-group">
                                                <label for="estadoPago">Estado de Pago</label>
                                                <select class="form-control" id="estadoPago" name="estado_pago">
                                                    <option value="0" selected>Pendiente</option>
                                                    <option value="1">Pagada</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex">
                                    <div class="ml-auto">
                                        <a href="#" class="btn btn-link" data-dismiss="modal">Cerrar</a>
                                        <button class="btn btn-primary px-4" type="submit"><span
                                                class="btn-inner--text">Reservar</span></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
@push('myjs')
    <script>
        function reservar(a, b) {
            dia = a;
            hora = b;
            document.getElementById('hora').value = hora
            document.getElementById('dia').value = dia

        }
    </script>
@endpush
