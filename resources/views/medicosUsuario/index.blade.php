@extends('plantillaUsuarios.principal')
@section('contenidoUsuarios')
    <link rel="stylesheet" type="text/css" href="{{ asset('assetsU/styles/news.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assetsU/styles/news_responsive.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assetsU/styles/about.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assetsU/styles/about_responsive.css') }}">
    <script src="{{ asset('assetsU/plugins/greensock/TweenMax.min.js') }}"></script>
    <script src="{{ asset('assetsU/plugins/greensock/TimelineMax.min.js') }}"></script>
    <script src="{{ asset('assetsU/plugins/scrollmagic/ScrollMagic.min.js') }}"></script>
    <script src="{{ asset('assetsU/plugins/greensock/animation.gsap.min.js') }}"></script>
    <script src="{{ asset('assetsU/plugins/greensock/ScrollToPlugin.min.js') }}"></script>
    <div class="home">
        <div class="elementor-kit-2891">
            <img class="mi-img" src="images/medicine2.svg" alt="" height="700">
        </div>
        <div class="mi-home_container">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="home_content">
                            <div class="home_title">Médicos</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="news">
        <div class="container">
            <div class="doctors">
                <div class="container">
                    <div class="row">
                        <div class="col text-center">
                            <div class="section_title">Nuestros Médicos</div>
                            <div class="section_subtitle">y especialidades</div>
                        </div>
                    </div>
                    <div class="alert alert-success m-5" role="alert">
                        Realize una reserva con el médico de su preferencia.
                    </div>
                    <div class="row doctors_row">
                        @foreach ($medicos as $medico)
                            <div class="offset-1 col-xl-3 col-md-6">
                                <div class="doctor">
                                    <div class="doctor_image"><img class="p-5" src="{{ asset('images/male.svg') }}"
                                            alt=""></div>
                                    <div class="doctor_content">
                                        <div class="doctor_name"><a href="#">{{ $medico->nombre }}</a></div>
                                        <div class="doctor_title">{{ $medico->especialidades_nombre }}</div>
                                        <div class="doctor_link"><a href="{{ route('medicosUsuario.show',$medico->id) }}">+</a></div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>

                    <div class="row">
                        <div class="col">
                            <div class="button doctors_button ml-auto mr-auto"><a href="#"><span>see all
                                        doctors</span><span>see all doctors</span></a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        .mi-home_container {
            position: absolute;
            top: 66%;
            left: 0;
            width: 100%;
        }

        .elementor-kit-2891 {

            height: 750px;
            background-image: linear-gradient(60deg, var(--teal) 0%, var(--secondary_color) 100%);
        }

        .mi-img {
            width: 227px;
            height: 200px;
            position: absolute;
            top: 25%;
            left: 50%;
        }

        :root {
            --brand_color: var(--accent_solid_color_opt, #10b3d6);
            --secondary_color: var(--secondary_color_opt, #1d2746);
            --p_color: var(--paragraph_color_opt, #6b707f);
        }

    </style>
@endsection
