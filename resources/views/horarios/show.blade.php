@extends('plantilla.principal')

@section('contenido')
    <div class="col">
        <div class="card bg-default shadow">
            <div class="card-header bg-transparent border-0">
                <div class="row">
                    <div class="col-9">
                        <h3 class="text-white mb-0">Horario de un medico</h3>
                        <h3 class="text-white mb-0">{{ $medico->nombre }}</h3>
                    </div>
                
                </div>
            </div>
            <div class="table-responsive">
                <table class="table align-items-center table-dark table-flush">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col" class="sort" data-sort="name">Dia</th>
                            <th scope="col" class="sort" data-sort="name">Estado</th>
                            <th scope="col" class="sort" data-sort="budget">Hora entrada</th>
                            <th scope="col" class="sort" data-sort="status">hora Salida</th>
                            
                        </tr>
                    </thead>
                    <tbody class="list">
                        @foreach ($horarios as $horario)
                            <tr>
                                <td class="budget">{{ $horario->dia}}</td>
                                <td class="budget">{{ $horario->estado }}</td>
                                <td class="budget">{{ $horario->hora_entrada }}</td>
                                <td class="budget">{{ $horario->hora_salida}}</td>
                                
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>



@endsection

@push('myjs')
@endpush
