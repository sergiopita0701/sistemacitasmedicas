@extends('plantilla.principal')

@section('contenido')
    <div class="col-xl-7">
        <div class="card">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-6">
                        <h3 class="my-5 ml-4">Editar Horario Medico</h3>
                    </div>
                </div>
                <form action="{{ route('horarios.update', $horario) }}" method="POST">
                    {{ csrf_field() }}
                    @method('PUT')
                    <div class="pl-lg-4 ml-4">
                        <div class="row">
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <label for="medico_id">Medico</label>
                                    <select class="form-control" id="medico_id" name="medico_id">
                                        @foreach ($medicos as $medico)
                                        <option value="{{ $medico->id }}" @if ($medico->id == $horario->medico_id) selected @endif>{{ $medico->nombre }}
                                        </option>
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <label for="dia">Dia</label>
                                    <select class="form-control" id="dia" name="dia" >
                                            <option value="lunes" selected>lunes</option>
                                            <option value="martes" >martes</option>
                                            <option value="miercoles">miercoles</option>
                                            <option value="jueves" >jueves</option>
                                            <option value="viernes" >viernes</option>
                                            <option value="sabado" >sabado</option>
                                            <option value="domingo" >domingo</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-5">
                                <div class="form-group">
                                    <label class="form-control-label" for="hora_entrada">Hora Ingreso</label>
                                    <input type="time" id="hora_entrada" name="hora_entrada" class="form-control" placeholder="hora_entrada" value="{{ $horario->hora_entrada }}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-5">
                                <div class="form-group">
                                    <label class="form-control-label" for="hora_salida">Hora Salida</label>
                                    <input type="time" id="hora_salida" name="hora_salida" class="form-control" placeholder="hora_salida" value="{{ $horario->hora_salida }}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="offset-4">
                            <button class="btn btn-icon btn-primary " type="submit">
                                <span class="btn-inner--icon"><i class="ni ni-fat-add"></i></span>
                                <span class="btn-inner--text">Agregar</span>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-xl-5">
        <div class="card">
            <div class="card-body">
                <img src="{{ asset('images\img2.svg') }} " class="ml-5" alt="" width="350px" height="300px">
            </div>
        </div>
    </div>

@endsection
