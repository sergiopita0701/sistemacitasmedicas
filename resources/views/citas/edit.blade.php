@extends('plantilla.principal')

@section('contenido')
    <div class="col-xl-7">
        <div class="card">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-6">
                        <h3 class="my-5 ml-4">Editar Cita</h3>
                    </div>
                </div>
                <form action="{{ route('citas.update', $cita) }}" method="POST">
                    {{ csrf_field() }}
                    @method('PUT')
                    <div class="pl-lg-4 ml-4">
                        <div class="row">
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <label class="form-control-label" for="asunto">Asunto</label>
                                    <input type="text" id="asunto" name="asunto" class="form-control"
                                        value="{{ $cita->asunto }}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <label for="paciente">Paciente</label>
                                    <select class="form-control" id="paciente" name="paciente_id">
                                        @foreach ($pacientes as $paciente)
                                            @if ($cita->paciente_id == $paciente->id)
                                                <option value="{{ $paciente->id }}" selected>{{ $paciente->nombre }}
                                                </option>
                                            @else
                                                <option value="{{ $paciente->id }}">{{ $paciente->nombre }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row ">
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <label for="user">Usuario</label>
                                    <select class="form-control" id="user" name="user_id">
                                        @foreach ($usuarios as $usuario)
                                            @if ($cita->user_id == $usuario->id)
                                                <option value="{{ $usuario->id }}" selected>{{ $usuario->name}}
                                                </option>
                                            @else
                                                <option value="{{ $usuario->id }}">{{ $usuario->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <label for="medico">Medico</label>
                                    <select class="form-control" id="medico" name="medico_id">
                                        @foreach ($medicos as $medico)
                                            @if ($cita->medico_id == $medico->id)
                                                <option value="{{ $medico->id }}" selected>{{ $medico->nombre }}
                                                </option>
                                            @else
                                                <option value="{{ $medico->id }}">{{ $medico->nombre }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-5">
                                <div class="form-group">
                                    <label class="form-control-label" for="fecha">Fecha</label>
                                    <input type="date" id="fecha" name="fecha_cita" class="form-control"
                                        value="{{ $cita->fecha_cita }}">
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <div class="form-group">
                                    <label class="form-control-label" for="hora">Hora</label>
                                    <input type="time" id="hora" name="hora_cita" class="form-control"
                                        value="{{ $cita->hora_cita }}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <label for="estadoCita">Estado de Cita</label>
                                    <select class="form-control" id="estadoCita" name="estado_cita">
                                        @if ($cita->estado_cita == 'Pendiente')
                                            <option value="Pendiente" selected>Pendiente</option>
                                            <option value="Cancelada">Cancelada</option>
                                            <option value="Activa">Activa</option>
                                        @endif
                                        @if ($cita->estado_cita == 'Cancelada')
                                            <option value="Pendiente">Pendiente</option>
                                            <option value="Cancelada" selected>Cancelada</option>
                                            <option value="Activa">Activa</option>
                                        @endif
                                        @if ($cita->estado_cita == 'Activa')
                                            <option value="Pendiente">Pendiente</option>
                                            <option value="Cancelada">Cancelada</option>
                                            <option value="Activa" selected>Activa</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <label class="form-control-label" for="precio">Precio</label>
                                    <input type="number" id="precio" name="precio" class="form-control"
                                        value="{{ $cita->precio }}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <label for="estadoPago">Estado de Pago</label>
                                    <select class="form-control" id="estadoPago" name="estado_pago">
                                        @if ($cita->estado_pago == 0)
                                            <option value="0" selected>Pendiente</option>
                                            <option value="1">Pagada</option>
                                        @else
                                            <option value="1" selected>Pagada</option>
                                            <option value="0">Pendiente</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="offset-4">
                            <button class="btn btn-icon btn-primary " type="submit">
                                <span class="btn-inner--icon"><i class="ni ni-fat-add"></i></span>
                                <span class="btn-inner--text">Agregar</span>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-xl-5">
        <div class="card">
            <div class="card-body">
                <img src="{{ asset('images\img1.svg') }} " class="ml-5" alt="" width="350px" height="300px">
            </div>
        </div>
    </div>

@endsection
