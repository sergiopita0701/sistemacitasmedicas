@extends('plantilla.principal')

@section('contenido')
    <div class="col">
        <div class="card bg-default shadow">
            <div class="card-header bg-transparent border-0">
                <div class="row">
                    <div class="col-9">
                        <h3 class="text-white mb-0">Lista Citas Registradas</h3>
                    </div>
                    <div class="col-3">
                        <a class="btn btn-icon btn-primary " type="button" href="{{ route('citas.create') }}">
                            <span class="btn-inner--icon"><i class="ni ni-fat-add"></i></span>
                            <span class="btn-inner--text">Agregar Nuevo</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table align-items-center table-dark table-flush">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col" class="sort" data-sort="name">Numero</th>
                            <th scope="col" class="sort" data-sort="name">Asunto</th>
                            <th scope="col" class="sort" data-sort="budget">Paciente</th>
                            <th scope="col" class="sort" data-sort="status">Medico</th>
                            <th scope="col" class="sort" data-sort="status">fecha/hora</th>
                            <th scope="col" class="sort" data-sort="status">Estado Cita</th>
                            <th scope="col" class="sort" data-sort="completion">Precio</th>
                            <th scope="col" class="sort" data-sort="completion">Estado Pago</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody class="list">
                        @foreach ($citas as $cita)
                            <tr>
                                <td class="budget">{{ $cita->id}}</td>
                                <td class="budget">{{ $cita->asunto }}</td>
                                <td class="budget">{{ $cita->paciente_nombre }}</td>
                                <td class="budget">{{ $cita->medico_nombre}}</td>
                                <td class="budget">{{ $cita->fecha_cita }}/{{ $cita->hora_cita }}</td>
                                <td class="budget">{{ $cita->estado_cita }}</td>
                                <td class="budget">{{ $cita->precio }}</td>
                                @if($cita->estado_pago == 1)
                                    <td class="budget">Pagado</td>
                                @else
                                    <td class="budget">Pendiente</td>
                                @endif
                                    <td class="text-right">
                                    <div class="dropdown">
                                        <a class="btn btn-sm btn-icon-only text-light" href="#" role="button"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-v"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                            <a class="dropdown-item" href="{{route('citas.edit',$cita->id)}}">Editar</a>
                                            <form  action="{{route('citas.destroy',$cita->id)}}" method="POST">
                                                {{csrf_field()}}
                                                @method('DELETE')
                                                <button  class="dropdown-item">Eliminar</button>
                                            </form>
                                            @if($cita->estado_cita == "Activa" || $cita->estado_cita == "Pendiente")
                                                <a class="dropdown-item" href="{{route('citasUsuario.edit',[$cita->id, 'estado'=>$cita->estado_cita])}}">Cancelar Cita</a>
                                            @else
                                                <a class="dropdown-item" href="{{route('citasUsuario.edit',[$cita->id, 'estado'=>$cita->estado_cita])}}">Activar Cita</a>
                                            @endif

                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('myjs')
@endpush
