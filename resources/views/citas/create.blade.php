@extends('plantilla.principal')

@section('contenido')
    <div class="col-xl-7">
        <div class="card">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-6">
                        <h3 class="my-5 ml-4">Registar Cita</h3>
                    </div>
                </div>
                <form action="{{ route('citas.store') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="pl-lg-4 ml-4">
                        <div class="row">
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <label class="form-control-label" for="asunto">Asunto</label>
                                    <input type="text" id="asunto" name="asunto" class="form-control" placeholder="asunto" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <label for="paciente">Paciente</label>
                                    <select class="form-control" id="paciente" name="paciente_id" required>
                                        @foreach ($pacientes as $paciente)
                                            <option value="{{ $paciente->id}}">{{ $paciente->nombre}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <label for="user">Usuario</label>
                                    <select class="form-control" id="user" name="user_id" required>
                                        @foreach ($pacientes as $paciente)
                                        <option value="{{ $paciente->user_id}}" selected>{{ $paciente->user_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <label for="medico">Medico</label>
                                    <select class="form-control" id="medico" name="medico_id" required>
                                        @foreach ($medicos as $medico)
                                            <option value="{{ $medico->id }}">{{ $medico->nombre }}</option>

                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-5">
                                <div class="form-group">
                                    <label class="form-control-label" for="fecha">Fecha</label>
                                    <input type="date" id="fecha" name="fecha_cita" class="form-control" placeholder="fecha" required>
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <div class="form-group">
                                    <label class="form-control-label" for="hora">Hora</label>
                                    <input type="time" id="hora" name="hora_cita" class="form-control" placeholder="hora" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <label for="estadoCita">Estado de Cita</label>
                                    <select class="form-control" id="estadoCita" name="estado_cita" required>
                                        <option value="Pendiente">Pendiente</option>
                                        <option value="Cancelada">Cancelada</option>
                                        <option value="Activa">Activa</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <label class="form-control-label" for="precio">Precio</label>
                                    <input type="number" id="precio" name="precio" class="form-control" placeholder="Precio" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <label for="estadoPago">Estado de Pago</label>
                                    <select class="form-control" id="estadoPago" name="estado_pago" required>
                                        <option value="0">Pendiente</option>
                                        <option value="1">Pagada</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="offset-4">
                            <button class="btn btn-icon btn-primary " type="submit">
                                <span class="btn-inner--icon"><i class="ni ni-fat-add"></i></span>
                                <span class="btn-inner--text">Agregar</span>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-xl-5">
        <div class="card">
            <div class="card-body">
                <img src="{{ asset('images\img2.svg') }} " class="ml-5" alt="" width="350px" height="300px">
            </div>
        </div>
    </div>

@endsection
