@extends('plantilla.principal')
@section('contenido')
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-6">
                        <h3 class="my-5 ml-4">Registar Cita</h3>
                    </div>
                </div>
                <div class="news">
                    <div class="container">
                        <div class="alert alert-danger" role="alert">
                            Puede cancelar hasta 24h antes de la cita
                          </div>
                        <div class="row">
                            <div class="col">
                                <div class="card">
                                    <div class="card-header border-0">
                                        <h3 class="mb-0">Lista de citas para </h3>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table align-items-center table-flush">
                                            <thead class="thead-light">
                                                <tr>
                                                    <th scope="col" class="sort">nombre</th>
                                                    <th scope="col" class="sort">Fecha/Hora</th>
                                                    <th scope="col" class="sort">Médico</th>
                                                    <th scope="col" class="sort">Descripcion</th>
                                                    <th scope="col" class="sort">Estado</th>
                                                    <th scope="col" class="sort">Precio</th>
                                                    <th scope="col" class="sort">estadoPrecio</th>
                                                    <th scope="col" class="sort"></th>
                                                </tr>
                                            </thead>
                                            <tbody class="list">
                                                @foreach ($citas as $cita)
                                                    <tr>
                                                        <td class="budget">{{ $cita->name }}</td>
                                                        <td class="budget">{{ $cita->fecha_cita }}/{{ $cita->hora_cita }}</td>
                                                        <td class="budget">{{ $cita->medico_nombre }}</td>
                                                        <td class="budget">{{ $cita->asunto }}</td>
                                                        <td class="budget">{{ $cita->estado_cita }}</td>
                                                        <td class="budget">{{ $cita->precio }}</td>
                                                        @if ($cita->estado_pago == 1)
                                                            <td class="budget">Pagado</td>
                                                        @else
                                                            <td class="budget">Pendiente</td>
                                                        @endif
                                                        @if($cita->estado_cita == "Activa" || $cita->estado_cita == "Pendiente")
                                                        <td class="budget">
                                                            <a class="btn btn-danger" href="{{route('citasUsuario.edit',[$cita->id, 'estado'=>$cita->estado_cita])}}">Cancelar</a>
                                                        </td>
                                                        @else
                                                        <td class="budget">
                                                            <a class="btn btn-success" href="{{route('citasUsuario.edit',[$cita->id, 'estado'=>$cita->estado_cita])}}">Activar</a>
                                                        </td>
                                                        @endif
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
