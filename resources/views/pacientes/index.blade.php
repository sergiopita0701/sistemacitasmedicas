@extends('plantilla.principal')

@section('contenido')
    <div class="col">
        <div class="card bg-default shadow">
            <div class="card-header bg-transparent border-0">
                <div class="row">
                    <div class="col-9">
                        <h3 class="text-white mb-0">Lista pacientes Registrados</h3>
                    </div>
                    <div class="col-3">
                        <a class="btn btn-icon btn-primary " type="button" href="{{ route('pacientes.create') }}">
                            <span class="btn-inner--icon"><i class="ni ni-fat-add"></i></span>
                            <span class="btn-inner--text">Agregar Nuevo</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table align-items-center table-dark table-flush">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col" class="sort" data-sort="name">Nombre</th>
                            <th scope="col" class="sort" data-sort="name">Apellido</th>
                            <th scope="col" class="sort" data-sort="budget">Genero</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody class="list">
                        @foreach ($pacientes as $paciente)
                            <tr>
                                <td class="budget">{{ $paciente->nombre }}</td>
                                <td class="budget">{{ $paciente->apellido }}</td>
                                <td class="budget">{{ $paciente->genero }}</td>
                                <td class="text-right">
                                    <div class="dropdown">
                                        <a class="btn btn-sm btn-icon-only text-light" href="#" role="button"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-v"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                            <a class="dropdown-item" href="{{ route('pacientes.show',$paciente) }}">toda la informacion</a>
                                            <a class="dropdown-item" href="{{ route('citas.show',$paciente->email) }}">ver citas</a>

                                            <a class="dropdown-item" href="{{ route('pacientes.edit',$paciente) }}">Editar</a>

                                            <form  action="{{route('pacientes.destroy',$paciente)}}" method="POST">
                                                {{csrf_field()}}
                                                @method('DELETE')
                                                <button  class="dropdown-item">Eliminar</button>
                                            </form>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>



@endsection

@push('myjs')
@endpush
