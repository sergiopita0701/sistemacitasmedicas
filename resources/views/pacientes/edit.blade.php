@extends('plantilla.principal')

@section('contenido')
    <div class="col-xl-7">
        <div class="card">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-6">
                        <h3 class="my-5 ml-4">Editar Paciente</h3>
                    </div>
                </div>
                <form action="{{ route('pacientes.update',$paciente) }}" method="POST">
                    {{ csrf_field() }}
                    @method('PUT')
                    <div class="pl-lg-4 ml-4">
                        <div class="row">
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <label class="form-control-label" for="nombre">Nombre</label>
                                    <input type="text" id="nombre" name="nombre" class="form-control" placeholder="nombre" value="{{ $paciente->nombre }}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <label class="form-control-label" for="apellido">Apellido</label>
                                    <input type="text" id="apellido" name="apellido" class="form-control" placeholder="apellido" value="{{ $paciente->apellido }}">
                                </div>
                            </div>
                        </div>
                        @if ($paciente->genero=='M')
                            @php ($hombre = 'checked')
                            @php ($mujer = '')
                        @else
                            @php ($hombre = '')
                            @php ($mujer = 'checked')
                        @endif
                        <div class="row">
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <label class="radio inline">
                                        <input type="radio" name="genero" value="M" {{ $hombre }}>
                                        <span> Masculino </span>
                                    </label>
                                    <label class="radio inline">
                                        <input type="radio" name="genero" value="F" {{ $mujer }}>
                                        <span>Femenino </span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-5">
                                <div class="form-group">
                                    <label class="form-control-label" for="fecha_nacimiento">Fecha de nacimento</label>
                                    <input type="date" id="fecha_nacimiento" name="fecha_nacimiento" class="form-control" placeholder="fecha_nacimiento" value="{{ $paciente->fecha_nacimiento }}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <label class="form-control-label" for="direccion">Direccion</label>
                                    <input type="text" id="direccion" name="direccion" class="form-control" placeholder="direccion" value="{{ $paciente->direccion }}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <label class="form-control-label" for="telefono">Telefono</label>
                                    <input type="text" id="telefono" name="telefono" class="form-control" placeholder="telefono" value="{{ $paciente->telefono }}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <label class="form-control-label" for="email">Email</label>
                                    <input type="text" id="email" name="email" class="form-control" placeholder="email" value="{{ $paciente->email }}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <label class="form-control-label" for="medicamento">Medicamentos</label>
                                    <textarea id="medicamento" name="medicamento" class="form-control" placeholder="medicamento">{{ $paciente->medicamento }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <label class="form-control-label" for="alergia">Alergias</label>
                                    <textarea id="alergia" name="alergia" class="form-control" placeholder="alergia">{{ $paciente->alergia }}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="offset-4">
                            <button class="btn btn-icon btn-primary " type="submit">
                                <span class="btn-inner--icon"><i class="ni ni-fat-add"></i></span>
                                <span class="btn-inner--text">Editar</span>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-xl-5">
        <div class="card">
            <div class="card-body">
                <img src="{{ asset('images\img2.svg') }} " class="ml-5" alt="" width="350px" height="300px">
            </div>
        </div>
    </div>

@endsection
