@extends('plantilla.principal')

@section('contenido')
    <div class="col-xl-7">
        <div class="card">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-6">
                        <h3 class="my-5 ml-4">Buscar citas de un Medico</h3>
                    </div>
                </div>
                <form action="{{ route('medicoCantidad.store') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="pl-lg-4 ml-4">
                       
                        <div class="row">
                            <div class="col-lg-5">
                                <div class="form-group">
                                    <label class="form-control-label" for="fecha">Fecha</label>
                                    <input type="date" id="fecha" name="fecha" class="form-control" placeholder="fecha">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <label for="medico">Medico</label>
                                    <select class="form-control" id="medico" name="medico_id">
                                        @foreach ($medicos as $medico)
                                            <option value="{{ $medico->id }}">{{ $medico->nombre }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="offset-4">
                            <button class="btn btn-icon btn-primary " type="submit">
                                <span class="btn-inner--icon"><i class="ni ni-fat-add"></i></span>
                                <span class="btn-inner--text">buscar</span>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-xl-5">
        <div class="card">
            <div class="card-body">
                <img src="{{ asset('images\img2.svg') }} " class="ml-5" alt="" width="350px" height="300px">
            </div>
        </div>
    </div>

@endsection
