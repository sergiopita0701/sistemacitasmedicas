@extends('plantilla.principal')

@section('contenido')
    <div class="col-xl-7">
        <div class="card">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-6">
                        <h3 class="my-5 ml-4">Horas Laborales Medico</h3>
                    </div>
                </div>

                <div class="pl-lg-4 ml-4">
                    <div class="row">
                        <div class="col-lg-10">
                            <div class="alert alert-warning" role="alert">
                                El medico debe completar 20hrs laborales a la semana
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pl-lg-4 ml-4">
                    <div class="row">
                        <div class="col-lg-10">
                            <div class="alert alert-info" role="alert">
                              Total hrs trabajadas cumplidas {{ $horas_cumplidas->hora }}
                            </div>
                        </div>
                    </div>
                </div>
                @if ($horas_cumplidas->hora <='20:00:00')
                    @php ($mensaje = 'no cumplio')
                    @php ($alert = 'danger')
                @else
                    @php ($mensaje = 'cumplio')
                    @php ($alert = 'success')
                @endif
                <div class="pl-lg-4 ml-4">
                    <div class="row">
                        <div class="col-lg-10">
                            <div class="alert alert-{{ $alert }}" role="alert">
                              {{ $mensaje}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="col-xl-5">
        <div class="card">
            <div class="card-body">
                <img src="{{ asset('images\img2.svg') }} " class="ml-5" alt="" width="350px" height="300px">
            </div>
        </div>
    </div>

@endsection
