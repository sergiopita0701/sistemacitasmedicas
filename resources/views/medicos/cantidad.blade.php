@extends('plantilla.principal')

@section('contenido')
    <div class="col-6">
        <div class="col">
            <div class="card bg-default shadow">
                <div class="card-header bg-transparent border-0">
                    <div class="row">
                        <div class="col-9">
                            <h3 class="text-white mb-0">Cantidad De citas (Reservas)</h3>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table align-items-center table-dark table-flush" id='medicos'>
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col" class="sort" data-sort="name">Nombre</th>
                                <th scope="col" class="sort" data-sort="name">Cantidad</th>
                            </tr>
                        </thead>
                        <tbody class="list" id='myTable'>
                            @foreach ($medicos as $medico)
                                <tr>
                                    <td class="budget">{{ $medico->nombre }}</td>
                                    <td class="budget">{{ $medico->cantidad_citas }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-6">
        <div class="card">
            <div class="card-body">
                <canvas id="myChart" width="400" height="400"></canvas>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js"></script>
    <script>
        let medicos_td = document.querySelectorAll('td');
        let citas_td = document.querySelectorAll('td + td');
        var citas = [];
        var medicos = [];
        for (let i = 0; i < medicos_td.length; i = i + 2) {
            medicos.push(medicos_td[i].firstChild.data);
        }
        for (let i = 0; i < citas_td.length; i++) {
            citas.push(parseInt(citas_td[i].firstChild.data));
        }

        var ctx = document.getElementById('myChart').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: medicos,
                datasets: [{
                    label: 'cantidad citas medicas',
                    data: citas,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }],
            },
        }
        });
    </script>
@endsection

@push('myjs')
@endpush
