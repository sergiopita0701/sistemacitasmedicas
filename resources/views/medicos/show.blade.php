
<a href="{{route('medicos.index')}}">volver al Registro</a>  
<br>
<a href="{{route('medicos.edit', $medico)}}" >Editar al medico</a>

@extends('plantilla.principal')

@section('contenido')
    <div class="col-xl-7">
        <div class="card">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-6">
                        <h3 class="my-5 ml-4">Mostrar Toda la informacion del paciente</h3>
                       <div class="pl-lg-4 ml-4">
                        <a class="btn btn-icon btn-secondary " href="{{route('medicos.index')}}">
                            <span class="btn-inner--text">Volver</span>
                        </a>
                        <a class="btn btn-icon btn-secondary " href="{{route('medicos.edit',$medico)}}">
                            <span class="btn-inner--text">Editar</span>
                        </a>
                       </div>
                    </div>
                </div>
                <form >
                    <div class="pl-lg-4 ml-4">
                        <div class="row">
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <label class="form-control-label" for="nombre">Nombre</label>
                                    <p class="text-primary" placeholder="nombre">{{ $medico->nombre }}</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <label class="form-check-label-label" for="apellido">Apellido</label>
                                    <p class="text-primary" placeholder="apellido">{{ $medico->apellido }}</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <label class="form-check-label-label" for="genero">Genero</label>
                                    <p class="text-primary" placeholder="genero">{{ $medico->genero }}</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-5">
                                <div class="form-group">
                                    <label class="form-check-label-label" for="fecha_nacimiento">Fecha De nacimiento</label>
                                    <p class="text-primary" placeholder="genero">{{ $medico->fecha_nacimiento }}</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <label class="form-check-label-label" for="direccion">Direccion</label>
                                    <p class="text-primary" placeholder="direccion">{{ $medico->direccion }}</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <label class="form-check-label-label" for="telefono">Telefono</label>
                                    <p class="text-primary" placeholder="telefono">{{ $medico->telefono }}</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <label class="form-check-label-label" for="telefono">Especialidad</label>
                                    <p class="text-primary" placeholder="especialidad_id">{{ $especialidad->nombre }}</p>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-xl-5">
        <div class="card">
            <div class="card-body">
                <img src="{{ asset('images\img2.svg') }} " class="ml-5" alt="" width="350px" height="300px">
            </div>
        </div>
    </div>

@endsection