@extends('plantilla.principal')

@section('contenido')
    <div class="col-xl-7">
        <div class="card">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-6">
                        <h3 class="my-5 ml-4">Registar Medico</h3>
                    </div>
                </div>
                <form action="{{ route('medicos.store') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="pl-lg-4 ml-4">
                        <div class="row">
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <label class="form-control-label" for="nombre">Nombre</label>
                                    <input type="text" id="nombre" name="nombre" class="form-control" placeholder="nombre" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <label class="form-control-label" for="apellido">Apellido</label>
                                    <input type="text" id="apellido" name="apellido" class="form-control" placeholder="apellido" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <div class="maxl">
                                        <label class="radio inline">
                                            <input type="radio" name="genero" value="M" checked>
                                            <span> Masculino </span>
                                        </label>
                                        <label class="radio inline">
                                            <input type="radio" name="genero" value="F" >
                                            <span> Femenino </span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-5">
                                <div class="form-group">
                                    <label class="form-control-label" for="fecha_nacimiento">Fecha de nacimento</label>
                                    <input type="date" id="fecha_nacimiento" name="fecha_nacimiento" class="form-control" placeholder="fecha_nacimiento" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <label class="form-control-label" for="direccion">Direccion</label>
                                    <input type="text" id="direccion" name="direccion" class="form-control" placeholder="direccion" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <label class="form-control-label" for="telefono">Telefono</label>
                                    <input type="text" id="telefono" name="telefono" class="form-control" placeholder="telefono" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <label for="medico">Especialidades</label>
                                    <select class="form-control" id="especialidad_id" name="especialidad_id" required>
                                        @foreach ($especialidades as $especialidad)
                                            <option value="{{ $especialidad->id }}">{{ $especialidad->nombre }}</option>

                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="offset-4">
                            <button class="btn btn-icon btn-primary " type="submit">
                                <span class="btn-inner--icon"><i class="ni ni-fat-add"></i></span>
                                <span class="btn-inner--text">Agregar</span>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-xl-5">
        <div class="card">
            <div class="card-body">
                <img src="{{ asset('images\img2.svg') }} " class="ml-5" alt="" width="350px" height="300px">
            </div>
        </div>
    </div>

@endsection
