<!DOCTYPE html>
<html lang="en">

<head>
    <title>Citas Médicas</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{ asset('assetsU/styles/bootstrap4/bootstrap.min.css') }}">
    <link href="{{ asset('assetsU/plugins/font-awesome-4.7.0/css/font-awesome.min.css') }} " rel="stylesheet"
        type="text/css">
    <link rel="stylesheet" type="text/css"
        href=" {{ asset('assetsU/plugins/OwlCarousel2-2.2.1/owl.carousel.css') }} ">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('assetsU/plugins/OwlCarousel2-2.2.1/owl.theme.default.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assetsU/plugins/OwlCarousel2-2.2.1/animate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assetsU/styles/main_styles.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assetsU/styles/responsive.css') }}">

</head>

<body>
    <div class="super_container">
        <div class="menu trans_500">
            <div class="menu_content d-flex flex-column align-items-center justify-content-center text-center">
                <div class="menu_close_container">
                    <div class="menu_close"></div>
                </div>
                <form action="#" class="menu_search_form">
                    <input type="text" class="menu_search_input" placeholder="Search" required="required">
                    <button class="menu_search_button"><i class="fa fa-search" aria-hidden="true"></i></button>
                </form>
                <ul>
                    <li class="menu_item"><a href="">Inicio</a></li>
                    <li class="menu_item"><a href="#">Médicos</a></li>
                </ul>
            </div>
            <div class="menu_social">
                <ul>
                    <li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-behance" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                </ul>
            </div>
        </div>

        <header class="header" id="header">
            <div>
                <div class="header_top">
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <div
                                    class="header_top_content d-flex flex-row align-items-center justify-content-start">
                                    <div class="logo">
                                        <a href="">Médicos<span>+</span></a>
                                    </div>
                                    <div
                                        class="header_top_extra d-flex flex-row align-items-center justify-content-start ml-auto">
                                        <div class="header_top_nav">

                                            <ul class="navbar-nav mr-auto">
                                            </ul>
                                            <ul class="d-flex flex-row align-items-center justify-content-start">
                                                @guest
                                                    @if (Route::has('login'))
                                                        <li class="nav-item">
                                                            <a class="nav-link"
                                                                href="{{ route('login') }}">{{ __('Inicio Sesion') }}</a>
                                                        </li>
                                                    @endif

                                                    @if (Route::has('register'))
                                                        <li class="nav-item">
                                                            <a class="nav-link"
                                                                href="{{ route('register') }}">{{ __('Registro') }}</a>
                                                        </li>
                                                    @endif
                                                @else
                                                    <li class="nav-item dropdown">
                                                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#"
                                                            role="button" data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false" v-pre>
                                                            {{ Auth::user()->name }}
                                                        </a>

                                                        <div class="dropdown-menu dropdown-menu-right"
                                                            aria-labelledby="navbarDropdown">
                                                            <a class="dropdown-item" href="{{ route('logout') }}"
                                                                onclick="event.preventDefault();
                                                                         document.getElementById('logout-form').submit();">
                                                                {{ __('Logout') }}
                                                            </a>

                                                            <form id="logout-form" action="{{ route('logout') }}"
                                                                method="POST" class="d-none">
                                                                @csrf
                                                            </form>
                                                        </div>
                                                    </li>
                                                @endguest
                                            </ul>

                                        </div>
                                    </div>
                                    <div class="hamburger ml-auto"><i class="fa fa-bars" aria-hidden="true"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="header_nav" id="header_nav_pin">
                    <div class="header_nav_inner">
                        <div class="header_nav_container">
                            <div class="container">
                                <div class="row">
                                    <div class="col">
                                        <div
                                            class="header_nav_content d-flex flex-row align-items-center justify-content-start">
                                            <nav class="main_nav">
                                                <ul class="d-flex flex-row align-items-center justify-content-start">
                                                    <li><a href="/">Inicio</a></li>
                                                    <li><a href="{{ route('medicosUsuario.index') }}">Médicos</a>
                                                    </li>
                                                    @if (Auth::check())
                                                        <li><a href="{{ route('citasUsuario.index') }}">Mis Citas</a>
                                                        </li>
                                                    @endif
                                                </ul>
                                            </nav>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        {{-- todo el contendo aqui --}}
        @yield('contenidoUsuarios')

        <footer class="footer">
            <div class="parallax_background parallax-window" data-parallax="scroll"
                data-image-src="{{ asset('assetsU/images/footer.jpg') }}" data-speed="0.8"></div>
            <div class="footer_content">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 footer_col">
                            <div class="footer_about">
                                <div class="logo">
                                    <a href="#">Médicos<span>+</span></a>
                                </div>
                                <div class="footer_about_text">Lorem ipsum dolor sit amet, lorem maximus consectetur
                                    adipiscing elit. Donec malesuada lorem maximus mauris.</div>
                                <div class="footer_social">
                                    <ul class="d-flex flex-row align-items-center justify-content-start">
                                        <li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-behance" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-4 footer_col">
                            <div class="footer_hours">
                                <div class="footer_hours_title">Horarios de Atencion</div>
                                <ul class="hours_list">
                                    <li class="d-flex flex-row align-items-center justify-content-start">
                                        <div>Lunes – viernes</div>
                                        <div class="ml-auto">8.00 – 19.00</div>
                                    </li>
                                    <li class="d-flex flex-row align-items-center justify-content-start">
                                        <div>Sabado</div>
                                        <div class="ml-auto">8.00 - 18.30</div>
                                    </li>
                                    <li class="d-flex flex-row align-items-center justify-content-start">
                                        <div>Domingo</div>
                                        <div class="ml-auto">9.30 – 17.00</div>
                                    </li>
                                    <li class="d-flex flex-row align-items-center justify-content-start">
                                        <div>Feriados</div>
                                        <div class="ml-auto">9.30 – 15.00</div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer_bar">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div
                                class="footer_bar_content d-flex flex-sm-row flex-column align-items-lg-center align-items-start justify-content-start">
                                <nav class="footer_nav">
                                    <ul
                                        class="d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-start">
                                        <li class="active"><a href="/">Inicio</a></li>
                                        <li><a href="about.html">Médicos</a></li>
                                        <li><a href="services.html">especialidades</a></li>
                                        <li><a href="news.html">Horarios</a></li>
                                    </ul>
                                </nav>
                                <div class="footer_links">
                                    <ul
                                        class="d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-start">
                                        <li><a href="#">Ayuda</a></li>
                                        <li><a href="#">Servicio de Emergencia 24h</a></li>
                                    </ul>
                                </div>
                                <div class="footer_phone ml-lg-auto">
                                    <i class="fa fa-phone" aria-hidden="true"></i>
                                    <span>+591 7894561</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>

    <script src="{{ asset('assetsU/js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('assetsU/styles/bootstrap4/popper.js') }}"></script>
    <script src="{{ asset('assetsU/styles/bootstrap4/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assetsU/plugins/scrollmagic/ScrollMagic.min.js') }} "></script>
    <script src="{{ asset('assetsU/plugins/OwlCarousel2-2.2.1/owl.carousel.js') }}"></script>
    <script src="{{ asset('assetsU/plugins/easing/easing.js') }}"></script>
    <script src="{{ asset('assetsU/plugins/parallax-js-master/parallax.min.js') }}"></script>
    <script src="{{ asset('assetsU/js/custom.js') }}"></script>
    <script src="{{ asset('assetsU/js/services.js') }}"></script>

</body>

<style>
    .header_top_nav ul li:not(:last-child)::after {
        content: '';
        margin-left: 0px;
        margin-right: 0px;
    }

</style>
@stack('myjs')

</html>
