@extends('plantilla.principal')

@section('contenido')
    <div class="col-xl-7">
        <div class="card">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-6">
                        <h3 class="my-5 ml-4">Editar Especialidades</h3>
                    </div>
                </div>
                <form action="{{ route('especialidades.update', $especialidad) }}" method="POST">
                    {{ csrf_field() }}
                    @method('PUT')
                    <div class="pl-lg-4 ml-4">
                        <div class="row">
                            <input type="hidden" id="id" name="id" class="form-control" value="{{ $especialidad->id }}">
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <label class="form-control-label" for="nombre">Especialidad</label>
                                    <input type="text" id="nombre" name="nombre" class="form-control" value="{{ $especialidad->nombre }}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <label class="form-control-label" for="precio">Precio</label>
                                    <input type="number" id="precio" name="precio" class="form-control" value="{{ $especialidad->precio }}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="offset-4">
                            <button class="btn btn-icon btn-primary " type="submit">
                                <span class="btn-inner--icon"><i class="ni ni-fat-add"></i></span>
                                <span class="btn-inner--text">Agregar</span>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-xl-5">
        <div class="card">
            <div class="card-body">
                <img src="{{ asset('images\img2.svg') }} " class="ml-5" alt="" width="350px" height="300px">
            </div>
        </div>
    </div>

@endsection
