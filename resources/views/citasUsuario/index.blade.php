@extends('plantillaUsuarios.principal')
@section('contenidoUsuarios')
    <link rel="stylesheet" type="text/css" href="{{ asset('assetsU/styles/news.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assetsU/styles/news_responsive.css') }}">
    <script src="{{ asset('assetsU/plugins/greensock/TweenMax.min.js') }}"></script>
    <script src="{{ asset('assetsU/plugins/greensock/TimelineMax.min.js') }}"></script>
    <script src="{{ asset('assetsU/plugins/scrollmagic/ScrollMagic.min.js') }}"></script>
    <script src="{{ asset('assetsU/plugins/greensock/animation.gsap.min.js') }}"></script>
    <script src="{{ asset('assetsU/plugins/greensock/ScrollToPlugin.min.js') }}"></script>
    <div class="home">
        <div class="elementor-kit-2891">
            <img class="mi-img" src="images/citas1.svg" alt="" height="700">
        </div>
        <div class="mi-home_container">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="home_content">
                            <div class="home_title">Citas</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="news">
        <div class="container">
            <div class="alert alert-danger" role="alert">
                Puede cancelar hasta 24h antes de la cita
            </div>
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header border-0">
                            <h3 class="mb-0">Lista de citas</h3>
                        </div>
                        <div class="table-responsive">
                            <table class="table align-items-center table-flush">
                                <thead class="thead-light">
                                    <tr>
                                        <th scope="col" class="sort">N°</th>
                                        <th scope="col" class="sort">Fecha/Hora</th>
                                        <th scope="col" class="sort">Médico</th>
                                        <th scope="col" class="sort">Descripcion</th>
                                        <th scope="col" class="sort">Estado</th>
                                        <th scope="col" class="sort">Precio</th>
                                        <th scope="col" class="sort">estadoPrecio</th>
                                        <th scope="col" class="sort"></th>
                                    </tr>
                                </thead>
                                <tbody class="list">
                                    @foreach ($citas as $cita)
                                        <tr>
                                            <td class="budget">{{ $cita->id }}</td>
                                            <td class="budget">{{ $cita->fecha_cita }}/{{ $cita->hora_cita }}</td>
                                            <td class="budget">{{ $cita->medico_nombre }}</td>
                                            <td class="budget">{{ $cita->asunto }}</td>
                                            <td class="budget">{{ $cita->estado_cita }}</td>
                                            <td class="budget">{{ $cita->precio }}</td>
                                            @if ($cita->estado_pago == 1)
                                                <td class="budget">Pagado</td>
                                            @else
                                                <td class="budget">Pendiente</td>
                                            @endif
                                            @if ($cita->estado_cita == 'Activa' || $cita->estado_cita == 'Pendiente')
                                                <td class="budget">
                                                    <a class="btn btn-danger"
                                                        href="{{ route('citasUsuario.edit', [$cita->id, 'estado' => $cita->estado_cita]) }}">Cancelar</a>
                                                </td>
                                            @else
                                                <td class="budget">
                                                    <a class="btn btn-success"
                                                        href="{{ route('citasUsuario.edit', [$cita->id, 'estado' => $cita->estado_cita]) }}">Activar</a>
                                                </td>
                                            @endif
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        .mi-home_container {
            position: absolute;
            top: 66%;
            left: 0;
            width: 100%;
        }

        .elementor-kit-2891 {

            height: 750px;
            background-image: linear-gradient(60deg, var(--teal) 0%, var(--secondary_color) 100%);
        }

        .mi-img {
            width: 227px;
            height: 200px;
            position: absolute;
            top: 25%;
            left: 50%;
        }

        :root {
            --brand_color: var(--accent_solid_color_opt, #10b3d6);
            --secondary_color: var(--secondary_color_opt, #1d2746);
            --p_color: var(--paragraph_color_opt, #6b707f);
        }

    </style>
@endsection
