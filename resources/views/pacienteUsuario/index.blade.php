<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('form/bootstrap.min.css') }} ">
    <link rel="stylesheet" href="{{ asset('form/style.css') }}">
    <title>Formulario Paciente</title>
</head>

<body>


    <div class="content">

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">


                    <div class="row justify-content-center">
                        <div class="col-md-5">

                            <h3 class="heading mb-4">Gracias por registrarte en nuestra pagina!</h3>
                            <p>Llena este formulario, para poder conocerte mejor!</p>

                            <p><img src="{{ asset('images/undraw-contact.svg') }}" alt="Image" class="img-fluid"></p>


                        </div>
                        <div class="col-md-7">
                            <form action="{{ route('pacienteUsuario.store') }}" method="POST">
                                {{ csrf_field() }}
                                <div class="pl-lg-4 ml-4">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class="form-control-label" for="nombre">Nombre</label>
                                                        <input type="text" id="nombre" name="nombre"
                                                            class="form-control" readonly value="{{ Auth::user()->name }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class="form-control-label"
                                                            for="apellido">Apellido</label>
                                                        <input type="text" id="apellido" name="apellido"
                                                            class="form-control" placeholder="apellido" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="row">
                                                <div class="col-lg-10">
                                                    <div class="form-group">
                                                        <div class="maxl">
                                                            <label class="radio inline">
                                                                <input type="radio" name="genero" value="M" checked>
                                                                <span> Masculino </span>
                                                            </label>
                                                            <label class="radio inline">
                                                                <input type="radio" name="genero" value="F">
                                                                <span> Femenino </span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class="form-control-label" for="fecha_nacimiento">Fecha
                                                            de
                                                            nacimento</label>
                                                        <input type="date" id="fecha_nacimiento" name="fecha_nacimiento"
                                                            class="form-control" placeholder="fecha_nacimiento" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class="form-control-label"
                                                            for="direccion">Direccion</label>
                                                        <input type="text" id="direccion" name="direccion"
                                                            class="form-control" placeholder="direccion" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class="form-control-label"
                                                            for="telefono">Telefono</label>
                                                        <input type="text" id="telefono" name="telefono"
                                                            class="form-control" placeholder="telefono" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-10">
                                            <div class="form-group">
                                                <label class="form-control-label" for="email">Email</label>
                                                <input type="email" id="email" name="email" class="form-control" readonly value="{{ Auth::user()->email }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class="form-control-label"
                                                            for="medicamento">Medicamentos</label>
                                                        <textarea id="medicamento" name="medicamento"
                                                            class="form-control" placeholder="medicamento" required></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class="form-control-label" for="alergia">Alergias</label>
                                                        <textarea id="alergia" name="alergia" class="form-control"
                                                            placeholder="alergia" required></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="offset-6">
                                        <button class="btn btn-icon btn-primary " type="submit">
                                            <span class="btn-inner--text ">Agregar</span>
                                        </button>
                                    </div>
                                </div>
                            </form>
                            <div id="form-message-warning mt-4"></div>
                            <div id="form-message-success">
                                Your message was sent, thank you!
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
