@extends('plantillaUsuarios.principal')
@section('contenidoUsuarios')
    <div class="home">
        <div class="background_image color-fondo">
            <img class="imagen-inicio" src="images/medicine4.svg" alt="" height="700" >
        </div>
        <div class="home_container">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="home_content">
                            <div class="home_title">Reserve sus citas con los mejores especialistas</div>
                            <div class="home_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec malesuada
                                lorem maximus mauris scelerisque, at rutrum nulla dictum. Ut ac ligula sapien.</div>
                            <div class="button home_button"><a href="#"><span>Leer mas</span><span>Leer mas</span></a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="services">
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <div class="section_title">Nuestros Servicios</div>
                    <div class="section_subtitle">Le ofrecemos Especialistas en:</div>
                </div>
            </div>
            <div class="row icon_boxes_row">
                <div class="col-xl-4 col-lg-6">
                    <div class="icon_box">
                        <div class="icon_box_title_container d-flex flex-row align-items-center justify-content-start">
                            <div class="icon_box_icon"><img src="assetsU/images/icon_1.svg" alt=""></div>
                            <div class="icon_box_title">Cardiologia</div>
                        </div>
                        <div class="icon_box_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec lorem
                            maximus malesuada lorem maximus mauris.</div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-6">
                    <div class="icon_box">
                        <div class="icon_box_title_container d-flex flex-row align-items-center justify-content-start">
                            <div class="icon_box_icon"><img src="assetsU/images/icon_2.svg" alt=""></div>
                            <div class="icon_box_title">Gastroenterología</div>
                        </div>
                        <div class="icon_box_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec lorem
                            maximus malesuada lorem maximus mauris.</div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-6">
                    <div class="icon_box">
                        <div class="icon_box_title_container d-flex flex-row align-items-center justify-content-start">
                            <div class="icon_box_icon"><img src="assetsU/images/icon_3.svg" alt=""></div>
                            <div class="icon_box_title">Laboratorio Medico</div>
                        </div>
                        <div class="icon_box_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec lorem
                            maximus malesuada lorem maximus mauris.</div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-6">
                    <div class="icon_box">
                        <div class="icon_box_title_container d-flex flex-row align-items-center justify-content-start">
                            <div class="icon_box_icon"><img src="assetsU/images/icon_4.svg" alt=""></div>
                            <div class="icon_box_title">Dentista</div>
                        </div>
                        <div class="icon_box_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec lorem
                            maximus malesuada lorem maximus mauris.</div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-6">
                    <div class="icon_box">
                        <div class="icon_box_title_container d-flex flex-row align-items-center justify-content-start">
                            <div class="icon_box_icon"><img src="assetsU/images/icon_5.svg" alt=""></div>
                            <div class="icon_box_title">Cirugía</div>
                        </div>
                        <div class="icon_box_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec lorem
                            maximus malesuada lorem maximus mauris.</div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-6">
                    <div class="icon_box">
                        <div class="icon_box_title_container d-flex flex-row align-items-center justify-content-start">
                            <div class="icon_box_icon"><img src="assetsU/images/icon_6.svg" alt=""></div>
                            <div class="icon_box_title">Neurologia</div>
                        </div>
                        <div class="icon_box_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec lorem
                            maximus malesuada lorem maximus mauris.</div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="button services_button ml-auto mr-auto"><a href="#"><span>Leer más</span><span>Leer
                                más</span></a></div>
                </div>
            </div>
        </div>
    </div>
    <style>
    .imagen-inicio {
    height: max-content;
    width: inherit;
    top: 130px;
    left: 260px;
    position: absolute;
}
.color-fondo{
    background: teal;
}
    </style>
@endsection
