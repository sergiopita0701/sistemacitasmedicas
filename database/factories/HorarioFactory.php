<?php

namespace Database\Factories;

use App\Models\Horario;
use Illuminate\Database\Eloquent\Factories\Factory;

class HorarioFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Horario::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'medico_id' => '1',
            'estado'=> '0',
            'dia'=>'lunes',
            'hora_entrada'=>'8:00:00',
            'hora_salida'=>'9:00:00',

        ];
    }
}
