<?php

namespace Database\Factories;

use App\Models\Paciente;
use Illuminate\Database\Eloquent\Factories\Factory;
use League\CommonMark\Block\Element\Paragraph;

class PacienteFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Paciente::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nombre'=> $this->faker->firstName(),
            'apellido'=> $this->faker->lastName(),
            'genero'=> $this->faker->randomElement(['M','F']),
            'fecha_nacimiento'=> $this->faker->date(),
            'direccion'=> $this->faker->address(),
            'telefono'=> $this->faker->phoneNumber(),
            'email' => $this->faker->unique()->safeEmail(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
            'medicamento'=> $this->faker->paragraph(),
            'alergia'=> $this->faker->paragraph(),
        ];
    }
}
