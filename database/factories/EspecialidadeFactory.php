<?php

namespace Database\Factories;

use App\Models\Especialidade;
use Illuminate\Database\Eloquent\Factories\Factory;

class EspecialidadeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Especialidade::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nombre'=> $this->faker->unique()->randomElement(['Dermatologia','Traumatologia','Endocrinología','Neumologia','Pediatria']),
            'precio'=> $this->faker->unique()->randomElement(['100','120','300','200','150']),
        ];
    }
}
