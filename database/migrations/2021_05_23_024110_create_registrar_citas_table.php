<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegistrarCitasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registrar_citas', function (Blueprint $table) {
            $table->id();
            $table->string('asunto');
            $table->integer('paciente_id');
            $table->integer('medico_id');
            $table->date('fecha_cita');
            $table->time('hora_cita');
            $table->string('estado_cita');
            $table->float('precio');
            $table->integer('estado_pago');
            $table->string('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registrar_citas');
    }
}
