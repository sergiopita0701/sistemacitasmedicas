<?php

namespace Database\Seeders;

use App\Models\Paciente;
use App\Models\RegistrarCita;
use App\Models\User;
use Illuminate\Database\Seeder;
use Prophecy\Call\Call;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        //\App\Models\Paciente::factory(10)->create();
        // $this->call(Paciente::class);
        $this->call(UserSeeder::class);
        $this->call(PacienteSeeder::class);
        $this->call(RegistrarCitaSeeder::class);
        $this->call(EspecialidadeSeeder::class);
        $this->call(MedicoSeeder::class);
        $this->call(HorarioSeeder::class);
    }
}
