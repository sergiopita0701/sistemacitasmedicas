<?php

use App\Http\Controllers\CitasUsuarioController;
use App\Http\Controllers\EspecialidadeController;
use App\Http\Controllers\PacienteController;
use App\Http\Controllers\RegistarCitaController;
use App\Http\Controllers\MedicoController;
use App\Http\Controllers\HorarioController;
use App\Http\Controllers\MedicoCantidadController;
use App\Http\Controllers\MedicoFechaController;
use App\Http\Controllers\MedicoUsuarioController;
use App\Http\Controllers\PacienteUsuarioController;
use App\Models\Especialidade;
use App\Models\RegistrarCita;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('inicioUsuarios/inicio');
});

Route::resource('pacientes', PacienteController::class);

Route::resource('citas',  RegistarCitaController::class);

Route::resource('citasUsuario',  CitasUsuarioController::class);
Route::resource('medicos', MedicoController::class);
#Route::resource('medicos.horarios', MedicoController::class);

Route::get('medicos/horas/{id}', [MedicoController::class, 'horas'])->name('medicos.horas');
Route::get('medicos/{id}/horas/', [MedicoController::class, 'horas'])->name('medicos.horas');
Route::post('medicos/graficas', [MedicoController::class, 'graficas'])->name('medicos.graficas');
Route::resource('medicoCantidad', MedicoCantidadController::class);
Route::resource('medicoFecha', MedicoFechaController::class);
//Route::get('medicoCantidad/fechaCantidad}', [MedicoCantidadController::class, 'fechaCantidad'])->name('medicoCantidad.fechaCantidad');
Route::resource('medicosUsuario', MedicoUsuarioController::class);

Route::resource('horarios', HorarioController::class);

Route::resource('especialidades', EspecialidadeController::class);

Route::resource('pacienteUsuario', PacienteUsuarioController::class);

Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

