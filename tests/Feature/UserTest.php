<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
    /** test carga de la url, ver el texto login */
    public function test_login_user()
    {
        $this->get('/login')
            ->assertStatus(200)
            ->assertSee('Login');
    }
    public function test_crear_usuario() {
        $this->assertDatabaseHas('users', [
            'name'=> 'admin',
            'email'=> 'admin@gmail.com',
            'password'=>'$2y$10$yQ68..nLYbVuLfPX48WwCO9R2rWhnrPS7wCzAgtgVLmyGa8lFvOK6',
            'rol'=> 1,
        ]);
    }
}
