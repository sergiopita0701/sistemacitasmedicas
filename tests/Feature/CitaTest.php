<?php

namespace Tests\Feature;

use App\Models\RegistrarCita;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CitaTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
      /** @test*/
      public function test_listar_cita()
      {
          $response = $this->get('/citas');
          $response->assertStatus(200);
      }
        /** @test*/
    public function store_cita()
    {
        $response = $this->post('/citas', [
            'asunto' => 'test asunto',
            'paciente_id' => '1',
            'medico_id' => '1',
            'fecha_cita' => '2021-07-05',
            'hora_cita' => '8:00:00',
            'estado_cita' => 'Activa',
            'precio' => '100',
            'estado_pago' => '0',
            'user_id' => '1',

        ]);
        $response->assertStatus(302);
        $this->assertDatabaseHas('registrar_citas', [
            'asunto' => 'test asunto',
            'paciente_id' => '1',
            'medico_id' => '1',
            'fecha_cita' => '2021-07-05',
            'hora_cita' => '8:00:00',
            'estado_cita' => 'Activa',
            'precio' => '100',
            'estado_pago' => '0',
            'user_id' => '1',
        ]);

    }

       /** @test*/
       public function delete_especialidad(){


        $this->withoutExceptionHandling();

        $cita = $this->post('/citas', [
            'id'=>'100',
            'asunto' => 'test asunto2',
            'paciente_id' => '1',
            'medico_id' => '1',
            'fecha_cita' => '2021-07-05',
            'hora_cita' => '8:00:00',
            'estado_cita' => 'Activa',
            'precio' => '100',
            'estado_pago' => '0',
            'user_id' => '1',

        ]);

        $this->delete("citas/100")
        ->assertRedirect('citas');

        $this->assertDatabaseMissing('especialidades',[
            'id'=>'100'
        ]);
        //$this->assertSame(0,Medico::count());
      }
}
