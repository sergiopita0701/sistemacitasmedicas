<?php

namespace Tests\Feature;

use App\Models\Horario;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class HorarioTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
    /** @test*/
    public function store_horario()
    {
        $response = $this->post('/horarios', [
            'medico_id' => 1,
            'dia'=> 'martes',
            'estado' => 0,
            'hora_entrada' => "08:00:00",
            'hora_salida' => "09:00:00",
        ]);
        $response->assertStatus(302);
        $this->assertDatabaseHas('horarios', [
            'medico_id' => 1,
            'dia'=> 'martes',
            'estado' => 0,
            'hora_entrada' => "08:00:00",
            'hora_salida' => "09:00:00",
        ]);
        
    }
    /** @test*/
    public function update_horario(){

        $horario=Horario::factory()->create([
            'medico_id' => 2,
            'dia'=> 'lunes',
            'estado' => 1,
            'hora_entrada' => "17:00:00",
            'hora_salida' => "18:00:00",
        ]);

        $response = $this->put("/horarios/$horario->id", [
            'medico_id' => 2,
            'dia'=> 'lunes',
            'estado' => 1,
            'hora_entrada' => "17:00:00",
            'hora_salida' => "18:00:00",
        ]);
        $response->assertStatus(302);
       

    }

      /** @test*/
    public function delete_horario(){

    
        $this->withoutExceptionHandling();

        $horario=Horario::factory()->create();

        $this->delete("horarios/{$horario->id}")
        ->assertRedirect('horarios');
        
        $this->assertDatabaseMissing('horarios',[
            'id'=>$horario->id
        ]);
        //$this->assertSame(0,Medico::count());
      }
}
