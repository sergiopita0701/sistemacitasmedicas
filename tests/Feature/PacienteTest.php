<?php

namespace Tests\Feature;

use App\Models\Paciente;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PacienteTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
      /** @test*/
      public function list_pacientes_test()
      {

          $response = $this->get('/pacientes');
          $response->assertStatus(200);
      }
    /** @test*/
    public function store_pacientes()
    {
        $response = $this->post('/pacientes', [
            'nombre' => 'paciente-test',
            'apellido' => 'apellido-test',
            'genero' => 'M',
            'fecha_nacimiento' => '1999-01-08',
            'direccion' => 'direccion-test',
            'telefono' => '8794556',
            'email' => 'paciente-test@gmail.com',
            'password' => bcrypt('123456'),
            'medicamento' => 'no',
            'alergia' => 'no'
        ]);
        // $response->assertStatus(302);
        $this->assertDatabaseHas('pacientes', [
            'nombre' => 'paciente-test',
            'apellido' => 'apellido-test',
            'genero' => 'M',
            'fecha_nacimiento' => '1999-01-08',
            'direccion' => 'direccion-test',
            'telefono' => '8794556',
            'email' => 'paciente-test@gmail.com',
            'password' =>'$2y$04$7YuuCwDG6K4aIU6g/pAOUOHsn3gJ6YvqbiFtRpClPBvBZy63YO.Ry',
            'medicamento' => 'no',
            'alergia' => 'no',
        ]);

    }

      /** @test*/
      public function delete_pacientes(){


        $this->withoutExceptionHandling();

        $paciente=Paciente::factory()->create();

        $this->delete("pacientes/{$paciente->id}")
        ->assertRedirect('pacientes');

        $this->assertDatabaseMissing('pacientes',[
            'id'=>$paciente->id
        ]);
      }

}
