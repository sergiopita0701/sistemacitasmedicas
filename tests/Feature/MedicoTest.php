<?php

namespace Tests\Feature;

use App\Models\Medico;
use Faker\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class MedicoTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /** @test*/
    public function list_medicos_test()
    {

        $response = $this->get('/medicos');
        $response->assertStatus(200);
    }
    /** @test*/
    public function store_medico()
    {
        $response = $this->post('/medicos', [
            'nombre' => 'blue',
            'apellido' => "Mna",
            'genero' => 'F',
            'fecha_nacimiento' => '1989-08-07',
            'direccion' => 'East Kristin, MO 83995',
            'telefono' => '564 481556',
            'especialidad_id' => 1
        ]);
        $response->assertStatus(302);
        $this->assertDatabaseHas('medicos', [
            'nombre' => 'blue',
            'apellido' => "Mna",
            'genero' => 'F',
            'fecha_nacimiento' => '1989-08-07',
            'direccion' => 'East Kristin, MO 83995',
            'telefono' => '564 481556',
            'especialidad_id' => 1
        ]);

    }
    /** @test*/
    public function update_medico(){

        $medico=Medico::factory()->create([
            'nombre' => '1',
            'apellido' => "2",
            'genero' => 'M',
            'fecha_nacimiento' => '1979-08-07',
            'direccion' => 'Kristin, MO 83995',
            'telefono' => '481556',
            'especialidad_id' => 2
        ]);

        $response = $this->put("/medicos/$medico->id", [
            'nombre' => 'blue',
            'apellido' => "Mna",
            'genero' => 'F',
            'fecha_nacimiento' => '1989-08-07',
            'direccion' => 'East Kristin, MO 83995',
            'telefono' => '564 481556',
            'especialidad_id' => 1
        ]);
        $response->assertStatus(200);
       
        /*
        $this->assertDatabaseHas('medicos', [
            'nombre' => 'blue',
            'apellido' => "Mna",
            'genero' => 'F',
            'fecha_nacimiento' => '1989-08-07',
            'direccion' => 'East Kristin, MO 83995',
            'telefono' => '564 481556',
            'especialidad_id' => 1
        ]);*/
        //$this->assertSame(0,Medico::count());
    }

      /** @test*/
    public function delete_medico(){

    
        $this->withoutExceptionHandling();

        $medico=Medico::factory()->create();

        $this->delete("medicos/{$medico->id}")
        ->assertRedirect('medicos');
        
        $this->assertDatabaseMissing('medicos',[
            'id'=>$medico->id
        ]);
        //$this->assertSame(0,Medico::count());
      }
}
