<?php

namespace Tests\Feature;

use App\Models\Especialidade;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class EspecialidadTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
    /** @test*/
    public function store_especialidad()
    {
        $response = $this->post('/especialidades', [
            'nombre' => 'Neurologia',
            'precio' => 66,
        ]);
        $response->assertStatus(302);
        $this->assertDatabaseHas('especialidades', [
            'nombre' => 'Neurologia',
            'precio' => 66,
        ]);
        
    }
    /** @test*/
    public function update_especialidad(){

        $especialidad=Especialidade::factory()->create([
            'nombre' => 'Endorologia',
            'precio' => 77,
        ]);

        $response = $this->put("/medicos/$especialidad->id", [
            'nombre' => 'Endorologia',
            'precio' => 77,
        ]);
        $response->assertStatus(200);
       

    }

      /** @test*/
    public function delete_especialidad(){

    
        $this->withoutExceptionHandling();

        $especialidad=Especialidade::factory()->create();

        $this->delete("especialidades/{$especialidad->id}")
        ->assertRedirect('especialidades');
        
        $this->assertDatabaseMissing('especialidades',[
            'id'=>$especialidad->id
        ]);
        //$this->assertSame(0,Medico::count());
      }
}
