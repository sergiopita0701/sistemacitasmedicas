<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RegistrarCita extends Model
{
    use HasFactory;
    protected $table = 'registrar_citas';
    protected $fillable = [
        'asunto',
        'paciente_id',
        'medico_id',
        'fecha_cita',
        'hora_cita',
       
        'estado_cita',
        'precio',
        'estado_pago',
        'user_id',
    ];
}
