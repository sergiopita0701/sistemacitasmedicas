<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class MedicoFechaController extends Controller
{
    
    public function create()
    {
        return view('medicos.cantidad_cita');
    }
    public function store(Request $request)
    {
        $medicos = DB::table('medicos')
        ->select(array('medicos.nombre', DB::raw('COUNT(registrar_citas.medico_id) as cantidad_citas')))
        ->leftJoin('registrar_citas', 'medicos.id', '=', 'registrar_citas.medico_id')
        ->whereBetween('registrar_citas.fecha_cita',array('2021-06-20','2021-06-24'))
        ->groupBy('medicos.nombre')
        ->orderBy('cantidad_citas', 'desc')
        ->get();
        return view('medicos.cantidad',['medicos' => $medicos]);
    }
}
