<?php

namespace App\Http\Controllers;

use App\Models\Paciente;
use App\Models\RegistrarCita;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;


class CitasUsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $citaPaciente = DB::table('registrar_citas')
            ->join('users', 'registrar_citas.user_id', '=', 'users.id')
            ->join('medicos', 'registrar_citas.medico_id', '=', 'medicos.id')
            ->select('registrar_citas.*', 'users.name', 'medicos.nombre as medico_nombre')
            ->where('users.name', '=', Auth::user()->name)
            ->get();
        return view('citasUsuario.index', ['citas' => $citaPaciente]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if(Auth::user()->rol == 1){
            $inputs = $request->all();
            $estado_cita = $inputs['estado'];
            if ($estado_cita == "Cancelada") {
                DB::table('registrar_citas')
                ->where('id', $id)
                ->update(['estado_cita' => 'Activa']);
            return Redirect::to('citas');
            } else {
                DB::table('registrar_citas')
                ->where('id', $id)
                ->update(['estado_cita' => 'Cancelada']);
            return Redirect::to('citas');
            }
        }else{
            $inputs = $request->all();
            $estado_cita = $inputs['estado'];
            if ($estado_cita == "Cancelada") {
                DB::table('registrar_citas')
                ->where('id', $id)
                ->update(['estado_cita' => 'Activa']);
            return Redirect::to('citasUsuario');
            } else {
                DB::table('registrar_citas')
                ->where('id', $id)
                ->update(['estado_cita' => 'Cancelada']);
            return Redirect::to('citasUsuario');
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
