<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Especialidade;
use App\Models\Horario;
use App\Models\Medico;
use Illuminate\Support\Facades\DB;
class MedicoCantidadController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }
   
    public function create()
    {
        $Medico = DB::table('medicos')
        ->select("*")
        ->get();
        return view('medicos.fecha', ['medicos' => $Medico]);
    }
    public function store(Request $request)
    {
        $fecha=$request['fecha'];
        $medico_id=$request['medico_id'];
        $citaPaciente = DB::table('registrar_citas')
        ->join('pacientes', 'registrar_citas.paciente_id', '=', 'pacientes.id')
        ->join('medicos', 'registrar_citas.medico_id', '=', 'medicos.id')
        ->select('registrar_citas.*','pacientes.nombre as paciente_nombre', 'medicos.nombre as medico_nombre')
        ->where('medico_id','=',$medico_id)
        ->where('fecha_cita','=',$fecha)
        ->get();
        return view('medicos.citas', ['citas' => $citaPaciente]);
    } 
    public function edit()
    {
        return view("medicos.fechas");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Medico  $medico
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $medicos = DB::table('medicos')
        ->select(array('medicos.nombre', DB::raw('COUNT(registrar_citas.medico_id) as cantidad_citas')))
        ->leftJoin('registrar_citas', 'medicos.id', '=', 'registrar_citas.medico_id')
        ->whereBetween('registrar_citas.fecha_cita',array('2021-06-20','2021-06-24'))
        ->groupBy('medicos.nombre')
        ->orderBy('cantidad_citas', 'desc')
        ->get();
        return view('medicos.cantidad',['medicos' => $medicos]);
    }
}
