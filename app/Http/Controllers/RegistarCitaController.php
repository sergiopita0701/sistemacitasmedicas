<?php

namespace App\Http\Controllers;

use App\Models\Paciente;
use App\Models\RegistrarCita;
use Database\Seeders\RegistrarCitaSeeder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;



class RegistarCitaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function index()
    {
        $citaPaciente = DB::table('registrar_citas')
        ->join('pacientes', 'registrar_citas.paciente_id', '=', 'pacientes.id')
        ->join('medicos', 'registrar_citas.medico_id', '=', 'medicos.id')
        ->select('registrar_citas.*','pacientes.nombre as paciente_nombre', 'medicos.nombre as medico_nombre')
        ->get();

        return view('citas.index', ['citas' => $citaPaciente]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Paciente = DB::table('pacientes')
        ->join('users', 'users.email', '=', 'pacientes.email')
        ->select('pacientes.*', 'users.id as user_id','users.email as user_email','users.name as user_name' )
        ->get();
        $Medico = DB::table('medicos')
        ->select("*")
        ->get();
        return view('citas.create', ['pacientes' => $Paciente, 'medicos' => $Medico]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs= $request->all();
        $date = $inputs['fecha_cita'];
        $newdate = date('Y/m/d', strtotime($date));
        $inputs['fecha_cita']= $newdate;
        $citas = RegistrarCita::create($inputs);
        $citas->save();
        return redirect()->route('citas.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($email)
    {
        $citaPaciente = DB::table('registrar_citas')
        ->join('users', 'registrar_citas.user_id', '=', 'users.id')
        ->join('medicos', 'registrar_citas.medico_id', '=', 'medicos.id')
        ->select('registrar_citas.*', 'users.name', 'medicos.nombre as medico_nombre')
        ->where('users.email', '=', $email)
        ->get();
        return view('citas.citasPaciente', ['citas' => $citaPaciente]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cita = RegistrarCita::findOrFail($id);

        $pacientes = DB::table('pacientes')
        ->select("*")
        ->get();
        $Medico = DB::table('medicos')
        ->select("*")
        ->get();
        $Usuario = DB::table('users')
        ->select("*")
        ->get();
        return view('citas.edit', ['cita' => $cita, 'pacientes'=>$pacientes, 'medicos' => $Medico,  'usuarios' => $Usuario]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RegistrarCita $cita)
    {
        $cita->fill($request->input());
        $cita->saveOrFail();
        return Redirect::to('citas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        RegistrarCita::destroy($id);
        return Redirect::to('citas');
    }
}
