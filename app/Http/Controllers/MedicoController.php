<?php

namespace App\Http\Controllers;

use App\Models\Especialidade;
use App\Models\Horario;
use App\Models\Medico;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MedicoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }
    public function index()
    {
        # $medicos=Medico::orderby('id','desc')->paginate();
        $medicos = Medico::orderby('id', 'desc')
            ->join('especialidades', 'medicos.especialidad_id', '=', 'especialidades.id')
            ->select('medicos.*', 'especialidades.nombre as especialidades_nombre')
            ->get();
        return view('medicos.index', ['medicos' => $medicos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $especialidades = DB::table('especialidades')
            ->select("*")
            ->get();
        return view("medicos.create", ['especialidades' => $especialidades]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->all();
        $medico = Medico::create($inputs);
        $medico->save();
        return redirect()->route('medicos.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Medico  $medico
     * @return \Illuminate\Http\Response
     */
    public function show(Medico $medico)
    {
        //
        $especialidad = Especialidade::findOrFail($medico->especialidad_id);

        return view("medicos.show", ['medico' => $medico, 'especialidad' => $especialidad]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Medico  $medico
     * @return \Illuminate\Http\Response
     */
    public function edit(Medico $medico)
    {
        //
        $especialidades = DB::table('especialidades')
            ->select("*")
            ->get();
        return view("medicos.edit", ['medico' => $medico, 'especialidades' => $especialidades]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Medico  $medico
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Medico $medico)
    {
        $especialidad = Especialidade::findOrFail($medico->especialidad_id);
        $medico->fill($request->input());
        $medico->saveOrFail();
        return view('medicos.show', ['medico' => $medico, 'especialidad' => $especialidad]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Medico  $medico
     * @return \Illuminate\Http\Response
     */
    public function destroy(Medico $medico)
    {
        $medico->delete();

        return redirect()->route('medicos.index');
    }
    
    public function horas($id)
    {
        $medico = Medico::findOrFail($id);
        $horas_cumplidas = DB::select('SELECT SEC_TO_TIME(SUM( TIME_TO_SEC(TIMEDIFF(hora_salida,hora_entrada))))as hora FROM horarios WHERE medico_id =' . $id);
        return view('medicos.horas', ['medico' => $medico, 'horas_cumplidas' => $horas_cumplidas[0]]);
    }
}
