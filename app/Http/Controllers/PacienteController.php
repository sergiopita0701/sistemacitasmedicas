<?php

namespace App\Http\Controllers;

use App\Models\Paciente;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;



class PacienteController extends Controller
{
    public function index()
    {
        $pacientes=Paciente::orderby('id','desc')->paginate();
        return view("pacientes.index",compact('pacientes'));
    }

    public function create()
    {
        return view("pacientes.create");
    }
    public function __construct()
    {
        // $this->middleware('auth');
    }
    public function store(Request $request)
    {
        $input = $request->all();
        $paciente = Paciente::create([
            'nombre' => $input['nombre'],
            'apellido' => $input['apellido'],
            'genero' => $input['genero'],
            'fecha_nacimiento' => $input['fecha_nacimiento'],
            'direccion' => $input['direccion'],
            'telefono' => $input['telefono'],
            'email' => $input['email'],
            'password' => bcrypt('123456'),
            'medicamento' => $input['medicamento'],
            'alergia' => $input['alergia'],
        ]);

        $usuarios = User::create([
            'name' => $input['nombre'],
            'email' => $input['email'],
            'password' => bcrypt('123456'),
        ]);
        $paciente->save();
        $usuarios->save();

        return redirect()->route('pacientes.show', $paciente);
    }

    public function show(Paciente $paciente)
    {
        return view("pacientes.show",compact('paciente'));
    }

    public function edit(Paciente $paciente)
    {
        return view('pacientes.edit',compact('paciente'));
    }

    public function update(Request $request, Paciente $paciente)
    {

        $paciente->fill($request->input());
        $paciente->saveOrFail();
        return view('pacientes.show',compact('paciente'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Paciente  $Paciente
     * @return \Illuminate\Http\Response
     */
    public function destroy(Paciente $paciente)
    {
        $paciente->delete();

        return redirect()->route('pacientes.index');

    }
}
