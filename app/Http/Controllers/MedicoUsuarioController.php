<?php

namespace App\Http\Controllers;

use App\Models\Paciente;
use App\Models\RegistrarCita;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Especialidade;
use App\Models\Medico;

class MedicoUsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $medicos = DB::table('medicos')
            ->join('especialidades', 'medicos.especialidad_id', '=', 'especialidades.id')
            ->select('medicos.*', 'especialidades.nombre as especialidades_nombre')
            ->get();
        return view('medicosUsuario.index', ['medicos' => $medicos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->all();
        $dia = $inputs['fecha_cita'];
        switch ($dia) {
            case 'lunes':
                $dia = 'Monday';
                break;
            case 'martes':
                $dia = 'Tuesday';
                break;
            case 'miercoles':
                $dia = 'Wednesday';
                break;
            case 'jueves':
                $dia = 'Thursday';
                break;
            case 'viernes':
                $dia = 'Friday';
                break;
            case 'sabado':
                $dia = 'Saturday';
                break;
            case 'domingo':
                $dia = 'Sunday';
                break;
            default:
                break;
        }
        $newdate = date('Y/m/d', strtotime('next ' . $dia . ' -1 week', strtotime('this Monday')));
        $inputs['fecha_cita']= $newdate;
        $citas = RegistrarCita::create($inputs);
        $citas->save();
        return redirect()->route('citasUsuario.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $medicos = DB::table('medicos')
            ->join('especialidades', 'medicos.especialidad_id', '=', 'especialidades.id')
            ->select('medicos.*', 'especialidades.nombre as especialidades_nombre')
            ->where('medicos.id', $id)
            ->get();
        $horarios = DB::table('medicos')
            ->join('horarios', 'horarios.medico_id', '=', 'medicos.id')
            ->select('horarios.*', 'medicos.nombre')
            ->where('horarios.medico_id', $id)
            ->get();
        $Paciente = DB::table('pacientes')
            ->join('users', 'users.email', '=', 'pacientes.email')
            ->select('pacientes.*', 'users.id as user_id', 'users.email as user_email', 'users.name as user_name')
            ->get();
        $Especialidad = DB::table('medicos')
            ->join('especialidades', 'medicos.especialidad_id', '=', 'especialidades.id')
            ->select('especialidades.*')
            ->where('medicos.id', $id)
            ->get();
        return view('medicosUsuario.show', ['medicos' => $medicos, 'horarios' => $horarios, 'pacientes' => $Paciente, 'especialidades' => $Especialidad]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
