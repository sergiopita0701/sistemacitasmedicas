<?php

namespace App\Http\Controllers;

use App\Models\Horario;
use App\Models\Medico;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HorarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $horarios = Horario::orderby('id', 'desc')
            ->join('medicos', 'horarios.medico_id', '=', 'medicos.id')
            ->select('horarios.*', 'medicos.nombre as medico_nombre')
            ->get();
        return view('horarios.index', ['horarios' => $horarios]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $medicos = DB::table('medicos')
            ->select("*")
            ->get();
        return view("horarios.create", ['medicos' => $medicos]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $input = $request->all();
        $horario = Horario::create([
            'medico_id' =>  $input['medico_id'],
            'dia' => $input['dia'],
            'estado' => 0,
            'hora_entrada' => $input['hora_entrada'],
            'hora_salida' => $input['hora_salida'],
        ]);
        $horario->save();

        return redirect()->route('horarios.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Horario  $horario
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $medico = Medico::findOrFail($id);
        // $horarios = Horario::orderby('id', 'desc');
        $horarios = Horario::orderby('id', 'asc')
            ->select('horarios.*')
            ->where('medico_id', '=', $id)
            ->get();
        //return $horarios;
        return view('horarios.show', ['horarios' => $horarios, 'medico' => $medico]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Horario  $horario
     * @return \Illuminate\Http\Response
     */
    public function edit(Horario $horario)
    {/*
        $medico = Medico::findOrFail($horario->medico_id);
        return view("horarios.show",['horario' => $horario,'medico' => $medico]);
*/
        $medicos = DB::table('medicos')
            ->select("*")
            ->get();
        return view("horarios.edit", ['horario' => $horario, 'medicos' => $medicos]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Horario  $horario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Horario $horario)
    {
        //$medio = Medico::findOrFail($horario->medico_id);
        $horario->fill($request->input());
        $horario->saveOrFail();
        return redirect()->route('horarios.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Horario  $horario
     * @return \Illuminate\Http\Response
     */
    public function destroy(Horario $horario)
    {
        $horario->delete();

        return redirect()->route('horarios.index');
    }
}
